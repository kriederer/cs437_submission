#!/usr/bin/env python
# coding: utf-8

# <p>
#     <img src="https://s3.amazonaws.com/iotanalytics-templates/Logo.png" style="float:left;width:65px">
#     <h1 style="float:left;color:#1A5276;padding-left:15px;font-size:20px;">AWS IoT Analytics | Notebook</h1>
# </p>
# 

# When loading data from IoT Analytics datasets, the client should be initialized first:

# In[2]:


import boto3

# create IoT Analytics client
client = boto3.client('iotanalytics')


# Now we can get the data location (URL) for the given dataset and start working with the data (In order to need to perform get_dataset_content, you need to grant iot analytics corresponding IAM permission):

# In[11]:


import pandas as pd
dataset = "vehicle_dataset"
dataset_url = client.get_dataset_content(datasetName = dataset)['entries'][0]['dataURI']

# start working with the data
data = pd.read_csv(dataset_url)
data = data.sort_values(by='timestep_time')
print(data)


# <div style="height:60px;"><div style="height:7px;background-color:#20B3CD;width:100%;margin-top:20px;position:relative;"><img src="https://s3.amazonaws.com/iotanalytics-templates/Logo.png" style="height:50px;width:50px;margin-top:-20px;position:absolute;margin-left:42%;"></div></div>

# In[26]:


import matplotlib.pyplot as plt

vehicle0 = data[data['carid']==0]
vehicle1 = data[data['carid']==1]
vehicle2 = data[data['carid']==2]
vehicle3 = data[data['carid']==3]
vehicle4 = data[data['carid']==4]

fig, ax = plt.subplots()

ax.plot(vehicle0['timestep_time'], vehicle0['vehicle_CO2'], color='green', label = 'Vehicle 0')
ax.plot(vehicle1['timestep_time'], vehicle1['vehicle_CO2'], color='red', label = 'Vehicle 1')
ax.plot(vehicle2['timestep_time'], vehicle2['vehicle_CO2'], color='blue', label = 'Vehicle 2')
ax.plot(vehicle3['timestep_time'], vehicle3['vehicle_CO2'], color='black', label = 'Vehicle 3')
ax.plot(vehicle4['timestep_time'], vehicle4['vehicle_CO2'], color='brown', label = 'Vehicle 4')
ax.set_title('Vehicle CO2 vs Time')
ax.set_xlabel('Time Step (s)')
ax.set_ylabel('CO2 Quantity')
ax.legend()
plt.show()


# In[54]:


fig, ax2 = plt.subplots()

ax2.boxplot(vehicle0['vehicle_CO2'], positions=[0], showfliers = False, medianprops={"color": "red", "linewidth": 0.5},
                whiskerprops={"color": "C0", "linewidth": 1.5},
                boxprops={'color': 'C0', "linewidth": 1.5},
                capprops={"color": "C0", "linewidth": 1.5})
ax2.boxplot(vehicle1['vehicle_CO2'], positions=[1], showfliers = False, medianprops={"color": "red", "linewidth": 0.5},
                whiskerprops={"color": "C0", "linewidth": 1.5},
                boxprops={'color': 'C0', "linewidth": 1.5},
                capprops={"color": "C0", "linewidth": 1.5})
ax2.boxplot(vehicle2['vehicle_CO2'], positions=[2], showfliers = False, medianprops={"color": "red", "linewidth": 0.5},
                whiskerprops={"color": "C0", "linewidth": 1.5},
                boxprops={'color': 'C0', "linewidth": 1.5},
                capprops={"color": "C0", "linewidth": 1.5})
ax2.boxplot(vehicle3['vehicle_CO2'], positions=[3], showfliers = False, medianprops={"color": "red", "linewidth": 0.5},
                whiskerprops={"color": "C0", "linewidth": 1.5},
                boxprops={'color': 'C0', "linewidth": 1.5},
                capprops={"color": "C0", "linewidth": 1.5})
ax2.boxplot(vehicle4['vehicle_CO2'], positions=[4], showfliers = False, medianprops={"color": "red", "linewidth": 0.5},
                whiskerprops={"color": "C0", "linewidth": 1.5},
                boxprops={'color': 'C0', "linewidth": 1.5},
                capprops={"color": "C0", "linewidth": 1.5})

ax2.set_title('Vehicle CO2 Statistics (Boxplot)')
ax2.set_xlabel('Vehicle Number')
ax2.set_ylabel('CO2 Quantity')
plt.show()


# In[ ]:




