import json
import logging
import sys

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")


max_CO2 = {}
def lambda_handler(event, context):
    global my_counter
    #TODO1: Get your data
    
    logger.debug(f'Event triggered={event}')
    car_dict = event
    carID = car_dict['carID']
    CO2 = float(car_dict['vehicle_CO2'])
    
    #TODO2: Calculate max CO2 emission
    if not carID in max_CO2:
        max_CO2[carID] = CO2
    else:
         max_CO2[carID] = max(CO2, max_CO2[carID])

    #TODO3: Return the result
    client.publish(
        topic=f"car/maxCO2/vehicle_{carID}",
        payload=json.dumps({"carID": carID, "max_CO2": max_CO2[carID]})
        )
    return