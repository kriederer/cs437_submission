################################################### Connecting to AWS
import boto3

import json
################################################### Create random name for things
import random
import string
import os

################################################### Parameters for Thing
thingArn = ''
thingId = ''
defaultPolicyName = 'testPolicy'
thing_group_arn = 'arn:aws:iot:us-east-1:137001575158:thinggroup/car_things'
###################################################
#./Certificates/device_{}/device_{}.
def createThing(thingName):
  global thingClient
  thingResponse = thingClient.create_thing(
      thingName = thingName
  )
  data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
  for element in data: 
    if element == 'thingArn':
        thingArn = data['thingArn']
    elif element == 'thingId':
        thingId = data['thingId']
  createCertificate(thingName)

def createCertificate(thingName):
    global thingClient
    certResponse = thingClient.create_keys_and_certificate(
            setAsActive = True
    )
    data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
    print(data)
    for element in data: 
            if element == 'certificateArn':
                    certificateArn = data['certificateArn']
            elif element == 'keyPair':
                    PublicKey = data['keyPair']['PublicKey']
                    PrivateKey = data['keyPair']['PrivateKey']
            elif element == 'certificatePem':
                    certificatePem = data['certificatePem']
            elif element == 'certificateId':
                    certificateId = data['certificateId']
                    print(f'Certificate ID={certificateId}')
                            
    with open(f'{thingName}_public.key', 'w') as outfile:
            outfile.write(PublicKey)
    with open(f'{thingName}_private.key', 'w') as outfile:
            outfile.write(PrivateKey)
    with open(f'{thingName}_cert.pem', 'w') as outfile:
            outfile.write(certificatePem)

    response = thingClient.attach_policy(
            policyName = defaultPolicyName,
            target = certificateArn
    )
    response = thingClient.attach_thing_principal(
            thingName = thingName,
            principal = certificateArn
    )
    response = thingClient.add_thing_to_thing_group(
        thingGroupArn=thing_group_arn,
        thingName=thingName
    )

thingClient = boto3.client('iot')
for i in range(1):
    thingName = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(15)])
    createThing(thingName)