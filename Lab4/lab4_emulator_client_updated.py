# Import SDK packages
import os
import sys
import time
import uuid
import json
import logging
import csv
import argparse
from AWSIoTPythonSDK.core.greengrass.discovery.providers import DiscoveryInfoProvider
from AWSIoTPythonSDK.core.protocol.connection.cores import ProgressiveBackOffCore
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from AWSIoTPythonSDK.exception.AWSIoTExceptions import DiscoveryInvalidRequestException
#import pandas as pd
#import numpy as np


#TODO 1: modify the following parameters
#Starting and end index, modify this
device_st = 0
device_end = 4

#Path to the dataset, modify this
data_path = "VehicleData/vehicle{}.csv"

#Path to your certificates, modify this
certificate_formatter = "./certs/{}-certificate.pem.crt"
key_formatter = "./certs/{}-private.pem.key"
clientId = '1f1747d77792eaa8c6ece84101de83f91b521d32cbb5f8a0adb9a05bcc828329'

YOUR_ENDPOINT = "a2s3e1xo488v6-ats.iot.us-east-1.amazonaws.com"
YOUR_ROOT_CA_PATH = './certs/AmazonRootCA1.pem'
MAX_DISCOVERY_RETRIES = 10
GROUP_CA_PATH = "./groupCA/"

carID = 4

host = YOUR_ENDPOINT
rootCAPath = YOUR_ROOT_CA_PATH
certificatePath = certificate_formatter.format(clientId)
privateKeyPath = key_formatter.format(clientId)
thingName = 'HelloWorld_Publisher'
published_topic = 'car/data'
subscribed_topic = f'car/maxCO2/vehicle_{carID}'
print_only = False

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.ERROR)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Progressive back off core
backOffCore = ProgressiveBackOffCore()

# Discover GGCs
discoveryInfoProvider = DiscoveryInfoProvider()
discoveryInfoProvider.configureEndpoint(host)
print(f'CAPath={rootCAPath}, certPath={certificatePath}, privateKey={privateKeyPath}')
discoveryInfoProvider.configureCredentials(rootCAPath, certificatePath, privateKeyPath)
discoveryInfoProvider.configureTimeout(10)  # 10 sec

retryCount = MAX_DISCOVERY_RETRIES if not print_only else 1
discovered = False
groupCA = None
coreInfo = None
while retryCount != 0:
    try:
        discoveryInfo = discoveryInfoProvider.discover(thingName)
        caList = discoveryInfo.getAllCas()
        coreList = discoveryInfo.getAllCores()

        # We only pick the first ca and core info
        groupId, ca = caList[0]
        coreInfo = coreList[0]
        print("Discovered GGC: %s from Group: %s" % (coreInfo.coreThingArn, groupId))

        print("Now we persist the connectivity/identity information...")
        groupCA = GROUP_CA_PATH + groupId + "_CA_" + str(uuid.uuid4()) + ".crt"
        if not os.path.exists(GROUP_CA_PATH):
            os.makedirs(GROUP_CA_PATH)
        groupCAFile = open(groupCA, "w")
        groupCAFile.write(ca)
        groupCAFile.close()

        discovered = True
        print("Now proceed to the connecting flow...")
        break
    except DiscoveryInvalidRequestException as e:
        print("Invalid discovery request detected!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))
        print("Stopping...")
        break
    except BaseException as e:
        print("Error in discovery!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))
        retryCount -= 1
        print("\n%d/%d retries left\n" % (retryCount, MAX_DISCOVERY_RETRIES))
        print("Backing off...\n")
        backOffCore.backOff()

if not discovered:
    # With print_discover_resp_only flag, we only woud like to check if the API get called correctly. 
    if print_only:
        sys.exit(0)
    print("Discovery failed after %d retries. Exiting...\n" % (MAX_DISCOVERY_RETRIES))
    sys.exit(-1)

# General message notification callback
def customOnMessage(client, userData, message):
    print('Received message on topic %s: %s\n' % (message.topic, message.payload))

# Iterate through all connection options for the core and use the first successful one
myAWSIoTMQTTClient = AWSIoTMQTTClient(thingName)
myAWSIoTMQTTClient.configureCredentials(groupCA, privateKeyPath, certificatePath)
#myAWSIoTMQTTClient.onMessage = customOnMessage

connected = False
for connectivityInfo in coreInfo.connectivityInfoList:
    currentHost = connectivityInfo.host
    currentPort = connectivityInfo.port
    print("Trying to connect to core at %s:%d" % (currentHost, currentPort))
    myAWSIoTMQTTClient.configureEndpoint(currentHost, currentPort)
    try:
        myAWSIoTMQTTClient.connect()
        connected = True
        break
    except BaseException as e:
        print("Error in connect!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))

if not connected:
    print("Cannot connect to core %s. Exiting..." % coreInfo.coreThingArn)
    sys.exit(-2)

myAWSIoTMQTTClient.subscribe(subscribed_topic, 0, customOnMessage)
time.sleep(2)

loopCount = 0
with open(data_path.format(carID)) as csvFile:
    csv_dict = csv.DictReader(csvFile, delimiter=',')

    for row in csv_dict:
        loopCount += 1
        #create dict with keys from csv header, values from row to send
        message = row
        message['carID'] = carID
        messageJson = json.dumps(message)
        myAWSIoTMQTTClient.publish(published_topic, messageJson, 0)
        print('Published topic %s: %s\n' % (published_topic, messageJson))
        
        time.sleep(1)





